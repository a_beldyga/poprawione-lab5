﻿using System;
using PK.Container;
using Wyswietlacz.Kontrakt;
using Glowny.Kontrakt;
using Wyswietlacz.Implementacja;
using Glowny.Implementacja;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IContainer container = new Kontener.Container();

            container.Register(System.Reflection.Assembly.Load("Glowny.Implementacja"));
            container.Register(System.Reflection.Assembly.Load("Wyswietlacz.Implementacja"));

            return container;
        }
    }
}
