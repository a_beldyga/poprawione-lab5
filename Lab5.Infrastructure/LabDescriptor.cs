﻿using System;
using System.Reflection;
using Kontener;
using Wyswietlacz.Implementacja;
using Wyswietlacz.Kontrakt;
using Glowny.Kontrakt;
using Glowny.Implementacja;


namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Kontener.Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(Glowny.Kontrakt.IGlowny));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Glowny.Implementacja.Glowny));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(Wyswietlacz.Kontrakt.IWyswietlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Wyswietlacz.Implementacja.Wyswietlacz));

        #endregion
    }
}
