﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyswietlacz.Kontrakt
{
    public interface IWyswietlacz
    {
        void Tekst(string tekst);
        void UstawKolorTla(string nazwaKoloru);
        void StworzViewModel();
    }
}
