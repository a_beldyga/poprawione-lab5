﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using System.Reflection;

namespace Kontener
{
    public class Container : IContainer
    {
        private Dictionary<Type, Type> _kontenerTypow = new Dictionary<Type, Type>();
        private Dictionary<Type, object> _kontenerObiektow = new Dictionary<Type, object>();

        public void Register<T>(Func<T> provider) where T : class
        {
            T obiekt = provider.Invoke();
            Register<T>(obiekt);
        }
        public void Register<T>(T impl) where T : class
        {
            Type[] interfejsy = impl.GetType().GetInterfaces();
            foreach (Type interfejs in interfejsy)
            {
                if (!_kontenerObiektow.ContainsKey(interfejs))
                {
                    _kontenerObiektow.Add(interfejs, impl);
                }
                else
                {
                    _kontenerObiektow[interfejs] = impl;
                }
            }
        }
        public void Register(Type type)
        {
            Type[] types = type.GetInterfaces();

            foreach (Type item in types)
            {
                if (_kontenerTypow.ContainsKey(item))
                {
                    _kontenerTypow[item] = type;
                }

                else
                {
                    _kontenerTypow.Add(item, type);
                }
            }
        }
        public void Register(Assembly assembly)
        {
            Type[] types;
            types = assembly.GetTypes();

            foreach (Type item in types)
            {
                if (!item.IsNotPublic)
                {
                    this.Register(item);
                }
            }
        }
        public object Resolve(Type type)
        {
            if (_kontenerObiektow.ContainsKey(type))
            {
                return _kontenerObiektow[type];
            }

            else if (!_kontenerTypow.ContainsKey(type))
            {
                return null;
            }

            else
            {
                ConstructorInfo[] ctors = _kontenerTypow[type].GetConstructors();

                foreach (ConstructorInfo item in ctors)
                {
                    ParameterInfo[] info = item.GetParameters();

                    if (info.Length == 0)
                    {
                        return Activator.CreateInstance(_kontenerTypow[type]);
                    }

                    List<object> list = new List<object>(info.Length);

                    foreach (ParameterInfo pinfo in info)
                    {
                        var p = Resolve(pinfo.ParameterType);

                        if (p == null)
                        {
                            throw new UnresolvedDependenciesException();
                        }

                        list.Add(p);
                    }

                    return item.Invoke(list.ToArray());
                }
            }

            return null;
        }
        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }
    }
}
