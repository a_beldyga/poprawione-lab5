﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wyswietlacz.Kontrakt;
using Glowny.Kontrakt;

namespace Glowny.Implementacja
{
    public class Glowny : IGlowny
    {
        private IWyswietlacz wyswietlacz;
        
        public Glowny(IWyswietlacz inter)
        {
            this.wyswietlacz = inter;
        }

        public void PrzekazNapis(string napis)
        {
            wyswietlacz.Tekst(napis);
        }
    
        public void PrzekazKolor(string nazwaKoloru)
        {
            wyswietlacz.UstawKolorTla(nazwaKoloru);
        }

        public void StworzViewModel()
        {
            wyswietlacz.StworzViewModel();
        }
    }
}
