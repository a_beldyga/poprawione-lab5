﻿using Lab5.DisplayForm;
using System;
using System.Collections.Generic;
using System.Windows;
using Wyswietlacz.Kontrakt;

namespace Wyswietlacz.Implementacja
{
    public class Wyswietlacz: IWyswietlacz
    {
        private DisplayViewModel _viewModel;
        
        public void StworzViewModel()
        {
            _viewModel = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                // utworzenie nowej formatki graficznej stanowiącej widok
                var form = new Form();
                // utworzenie modelu widoku (wzorzec MVVM)
                var viewModel = new DisplayViewModel();
                // przypisanie modelu do widoku
                form.DataContext = viewModel;
                // wyświetlenie widoku
                form.Show();
                // zwrócenie modelu widoku do dalszych manipulacji
                return viewModel;
            }), null);
        }

        public void Tekst(string tekst)
        {
            _viewModel.Text = tekst;
        }


        public void UstawKolorTla(string nazwaKoloru)
        {
            _viewModel.Kolor = nazwaKoloru;
        }
    }
}
