﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glowny.Kontrakt
{
    public interface IGlowny
    {
        void StworzViewModel();
        void PrzekazNapis(string napis);
        void PrzekazKolor(string nazwaKoloru);
    }
}
